<?php

// Create hook funktion that alters the sql. It takes two args. $where (the sql string) and $wp_query (which is the actual query and holds the args for the function, $beinDate and $endDate).
function posts_where_hook( $where = '', $wp_query )
{
	
	$beginDate = $wp_query->get("beginDate");
	$endDate = $wp_query->get("endDate");

	$where .= " AND post_date >= '".$beginDate."' AND post_date < '".$endDate."'";
	
	return $where;
}

// Add hook
add_filter( 'posts_where', 'posts_where_hook', 10, 2 );

// Run the query. The two last arguments will be accessed by the hook using $wp_query->get
query_posts( "cat=-5&beginDate=".$beginDate."&endDate=".$endDate);

?>