<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dgstz
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width">
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 
<?php wp_head(); ?>
</head>

<body >
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'dgstz' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<!-- <div class="headerr">

  <div class="search-logo">


			<div class="search">
				<div class="contact">
				    <p><a href="http://dgstz.ge/contact/">კონტაქტი</a></p>
				</div>
				<! <div class="font-search">
				<a onclick="jQuery('.search-submit').submit();"><i class="fa fa-search" type="submit"></i></a>
				</div>  --> 
	<!-- 			
	  <section id="search-2" class="widget widget_search">
	  <form id="search-form" role="search" method="get" class="search-form" action="http://dgstz.ge/">
				
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="search" class="search-field" placeholder="მოძებნე სახელით, ავტორით ან" value="" name="s" title="Search for:">
				
			</label>
			<button type="submit" class="search-submit"><span class="fa fa-search"></span></button>
			</form> -->
			 
				<!-- <input type="submit" class="search-submit" value="Search">
			</form></section> -->
		<!-- <input class="search-input" placeholder="მოძებნე სახელით, ავტორით ან"/>
		<div class="language-switcher">
			   <h3>ქართ</h3>
			    <h3>DEU</h3>
		</div>  -->




<!-- <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="screen-reader-text">Search for:</span>
		<input type="search" class="search-field" placeholder="Search …" value="" name="s" title="Search for:" />
	</label>
	<input type="submit" class="search-submit" value="Search" />
</form> -->










		
	</div>
	
			
				

		<?php	$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>

		</div><!-- .site-branding -->


		

		<nav id="site-navigation" class="main-navigation" role="navigation">

		<div class="container">


			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'sds' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>

		</div>

		</nav><!-- #site-navigation -->
		<div class="container">
		<?php do_action('wpml_add_language_selector'); ?>
		</div>


	
		<div class="logo">
		   <?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
		   	<div class="container">
    <div class='site-logo'>
        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
        
        <div class="header-p">
        <h5>DEUTSCH-GEORGISCHE STRAFRECHTSZEITSCHRIFT</h5>
    	<h5>გერმანულ-ქართული სისხლის სამართლის ჟურნალი</h5>
    </div>

    </div>

<?php else : ?>
    <hgroup>
        <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
        <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
    </hgroup>
<?php endif; ?>


		</div>

	</div>
	</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">



<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dgstz
 */
