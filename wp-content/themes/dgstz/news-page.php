<?php
/*
Template Name: news Page
*
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

get_header(); ?>
<?php get_sidebar('left'); ?>
	<div id="row">
		<div class="side1 col-md-6">

	            <div class="entry">



		
		<?php $loop = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 10 ) ); ?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
  


	<?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>
<?php echo get_the_time('Y-m-d', $single_post->ID); //The date is on Y-m-d format
    echo '<br />' ; ?>


	<div class="entry-content">
		<?php the_content(); ?>
	</div>
<?php endwhile; ?>
		</div>
		</div>
		</div>
		

			

		

		<?php get_sidebar(); ?>
		


<?php get_footer(); ?>
