<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dgstz
 */

if ( ! is_active_sidebar( 'row' ) ) {
	return;
}

?>

<!-- #secondary -->



			
		
	<div class="left-sidebar col-md-3">
<aside id="secondary" class="widget-area left" role="complementary">

				
			 
	
	<?php dynamic_sidebar( 'row' ); ?>
<div id="footer-sidebar">
	<?php
if(is_active_sidebar('bottom-sidebar')){
dynamic_sidebar('bottom-sidebar');
}
?>
</div>

<?php 
    $args = array(
	'show_option_all'    => '',
	'orderby'            => 'name',
	'order'              => 'ASC',
	'style'              => 'list',
	'show_count'         => 0,
	'hide_empty'         => 1,
	'use_desc_for_title' => 0,
	'child_of'           => 0,
	'feed'               => '',
	'feed_type'          => '',
	'feed_image'         => '',
	'exclude'            => '',
	'exclude_tree'       => '',
	'include'            => '',
	'hierarchical'       => 1,
	'title_li'           => __( 'ჟურნალის გამომცემლები' ),
	'show_option_none'   => __( '' ),
	'number'             => null,
	'echo'               => 1,
	'depth'              => 0,
	'current_category'   => 0,
	'pad_counts'         => 0,
	'taxonomy'           => 'category',
	'walker'             => null,
        'include' => '24,25,20,26,23,15.22, 21,13'
    );
    wp_list_categories( $args ); 
?>


			
</aside>
		
	</div>

	

	



		
	




