<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dgstz
 */

get_header(); ?>
<?php get_sidebar('left'); ?>
	
	
		<div class="side1 col-md-6">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			

			

		<?php endwhile; // End of the loop. ?>

	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
