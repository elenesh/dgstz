<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

get_header(); ?>


<?php get_sidebar('left'); ?>
	<div id="row">
		<div class="side1 col-md-6">
                       <select name="archive-dropdown" class="archive-select" onchange="document.location.href=this.options[this.selectedIndex].value;">
  <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
</select>
		<div class="entry">
			<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

 <?php echo get_the_time('Y-m-d', $single_post->ID); //The date is on Y-m-d format
    echo '<br />' ; ?>


		<?php endwhile; // End of the loop. ?>
		</div>
		</div>
		</div>

	
<?php get_sidebar(); ?>
<?php get_footer(); ?>
