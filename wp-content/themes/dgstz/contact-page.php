<?php
/*
Template Name: contact Page
*
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

get_header(); ?>


	<?php get_sidebar('left'); ?>

<div class="side1 col-md-6">


			<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>


		<?php endwhile; // End of the loop. ?>




    <div class="clear"></div>
</div>

		

			

		

		<?php get_sidebar(); ?>
		<?php get_footer(); ?>	




		


<?php get_footer(); ?>
