<?php
/**
 * dgstz functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package dgstz
 */

if ( ! function_exists( 'dgstz_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function dgstz_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on dgstz, use a find and replace
	 * to change 'dgstz' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'dgstz', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */

	add_theme_support( 'site-logo' );

	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'dgstz' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'dgstz_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // dgstz_setup
add_action( 'after_setup_theme', 'dgstz_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function themeslug_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );
    $wp_customize->add_setting( 'themeslug_logo' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'themeslug_logo_section',
    'settings' => 'themeslug_logo',
) ) );
}
add_action( 'customize_register', 'themeslug_theme_customizer' );


// Changing excerpt length
function new_excerpt_length($length) {
return 100;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Changing excerpt more
function new_excerpt_more($more) {
return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');




function dgstz_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'dgstz_content_width', 640 );
}
add_action( 'after_setup_theme', 'dgstz_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function dgstz_widgets_init() {
	if ( function_exists('register_sidebar') )
	register_sidebar( array(
		'name'          => esc_html__( 'left-Sidebar', 'dgstz' ),
		'id'            => 'row',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title left">',
		'after_title'   => '</h3>',
	) );


register_sidebar(array('name'=>'left',
'before_widget' => '<li id="%1$s" class="widget %2$s">',
'after_widget' => '',
'before_title' => '<h2 class="widgettitle">',
'after_title' => '</h2>',
));


register_sidebar(array('name'=>'bottom-sidebar',
'before_widget' => '<li id="%1$s" class="widget %2$s">',
'after_widget' => '',
'before_title' => '<h2 class="widgettitle">',
'after_title' => '</h2>',
));

}
add_action( 'widgets_init', 'dgstz_widgets_init' );


 wp_enqueue_script( 'googleapis', 'https://maps.googleapis.com/maps/api/js?sensor=false');


add_action( 'init', 'create_my_post_types' );

function create_my_post_types() {
	register_post_type( 'aqtualuri-kanon', 
		array(
			'labels' => array(
				'name' => __( 'aqtualuri-kanon' ),
				'singular_name' => __( 'aqtualuri-kanon' )
			),
			 'taxonomies' => array('category'), 
			'public' => true,
		)
	);



	

	register_post_type( 'gamomcemlebi', 
		array(
			'labels' => array(
				'name' => __( 'gamomcemlebi' ),
				'singular_name' => __( 'gamomcemle' )
			),

			 'taxonomies' => array('category'), 

			'public' => true,
		)
	);


	register_post_type( 'publikaciebi', 
		array(
			'labels' => array(
				'name' => __( 'publikaciebi' ),
				'singular_name' => __( 'publikaciebi' )
			),

			 'taxonomies' => array('category'), 

			'public' => true,
		)
	);


	register_post_type( 'law', 
		array(
			'labels' => array(
				'name' => __( 'law' ),
				'singular_name' => __( 'law' )
			),

			 'taxonomies' => array('category'), 

			'public' => true,
		)
	);

register_post_type( 'blog', 
		array(
			'labels' => array(
				'name' => __( 'blog' ),
				'singular_name' => __( 'blog' )
			),

			 'taxonomies' => array('category'), 

			'public' => true,
		)
	);

}

// Our custom post type function
function create_posttype() {

	register_post_type( 'news',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'news' ),
				'singular_name' => __( 'news' )
			),
			'public' => true,
			'has_archive' => true,
			
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}





/**
 * Enqueue scripts and styles.
 */

function register_my_theme_styles(){
        if ( ! is_admin() ){
                wp_register_style( 'my-theme-stylesheet', get_template_directory_uri() . '/css/mystyle.css', array(), false, 'screen' );
        }
}
add_action( 'init', 'register_my_theme_styles' );

function enqueue_my_theme_styles(){
        if ( ! is_admin() ){
                wp_enqueue_style( 'my-theme-stylesheet' );
        }
}
add_action( 'wp_enqueue_scripts', 'enqueue_my_theme_styles' );


function dgstz_scripts() {
	wp_enqueue_style( 'dgstz-style', get_stylesheet_uri() );
wp_enqueue_style( 'dgstz-style', get_stylesheet_uri(). '/css/mystyle.css', array(), '20120206', true  );

	wp_enqueue_script( 'dgstz-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'dgstz-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
    
    wp_enqueue_script( 'dgstz-navigation', get_template_directory_uri() . '/bootstrap/js/bootrstrap.min.js', array(), '20120206', true );
     wp_enqueue_script( 'dgstz-navigation', get_template_directory_uri() . '/bootstrap/js/bootrstrap.js', array(), '20120206', true );
	
	 wp_enqueue_script( 'dgstz-navigation', get_template_directory_uri() . 'http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js', array(), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'dgstz_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

$query_string = "order=ASC&posts_per_page=-1";

// Create a new filtering function that will add our where clause to the query
function filter_where( $where = '' ) {
    $where .= " AND post_date >= '2012-03-01'";
    return $where;
}

add_filter( 'posts_where', 'filter_where' );
$custom_query = new WP_Query( $query_string );
remove_filter( 'posts_where', 'filter_where' );