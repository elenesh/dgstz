<?php
/*
Template Name: publications Page
*
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

get_header(); ?>
<?php get_sidebar('left'); ?>
	<div id="row">
		<div class="side1 col-md-6">

			<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>


		<?php endwhile; // End of the loop. ?>
		
		
		

<?php $loop = new WP_Query( array( 'post_type' => 'publikaciebi', 'posts_per_page' => '10', 'category_name' => single_cat_title('წიგნების რეცენზიები', true)) ); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>
<?php endwhile; ?>
		</div>
		</div>
		

			

		

		<?php get_sidebar(); ?>
		


<?php get_footer(); ?>
