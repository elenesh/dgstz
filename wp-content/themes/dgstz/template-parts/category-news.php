<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php dgstz_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php 
        $reviewArgs = array( 
            'category' => '3', 
            'posts_per_page' => 5
        );
        $reviews = get_posts( $reviewArgs );
        foreach ($reviews as $post) :  setup_postdata($post); 
            the_post_thumbnail();
            the_title();
            the_content();
        endforeach; 
?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dgstz' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dgstz_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
