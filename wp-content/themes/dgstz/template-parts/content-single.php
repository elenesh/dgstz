<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="post">
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
				<div class="entry"><?php the_content(); ?>
</div>
			</div>
		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dgstz_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
