<?php
/*
Template Name: home Page
*
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dgstz
 */

get_header(); ?>
	<?php get_sidebar('left'); ?>
		<div class="side1 col-md-6">
		<div class="entry">
		<?php echo do_shortcode('[wonderplugin_slider id="1"]'); ?>
		</div>



			<?php while ( have_posts() ) : the_post(); ?>

 
			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>




		<?php endwhile; // End of the loop. ?>

		

	
<div class="side1 col-md-12">
		
   
     <h2 class="top with-background" style="margin-left:-18px;"><a href="<?php echo get_post_type_archive_link( 'publikaciebi' ); ?>"><?php _e('ახალი პუბლიკაციები','dgstz'); ?></a></h2>
        
         
       <?php $loop = new WP_Query( array( 'post_type' => 'publikaciebi', 'posts_per_page' => 3 ) ); ?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>



	<?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>

<div class="entry-content">
		<?php the_excerpt(); ?>
	</div>

<?php endwhile; ?>

   
    


</div>


<h2 class="with-background"><?php _e('აქტუალური კანონი','dgstz'); ?></h2>
		<?php $loop = new WP_Query( array( 'post_type' => 'law', 'posts_per_page' => 3 ) ); ?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>



	<?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>

<div class="entry-content">
		<?php the_excerpt(); ?>
	</div>

	
<?php endwhile; ?>


		</div>


		






		<?php get_sidebar(); ?>	

		<?php get_sidebar( 'bottom-sidebar' ); ?>
			
			<?php get_footer(); ?>	
		
		
	



