

<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package dgstz
 */

get_header(); ?>
<?php get_sidebar('left'); ?>






	

<script>
  $(function() {
   $('.datepicker').datepicker( {
   
});
     $('.datepicker2').datepicker( {
   
});
  });

  </script>


<?php
$meta_key1		= $_GET['sDate']; 
$meta_key2		=   $_GET['eDate']; 

// echo $meta_key1;
$year_start=(int)explode("-",$meta_key1)[0];
$year_end=(int)explode("-",$meta_key2)[0];
$month_start=(int)explode("-",$meta_key1)[1];
$month_end=(int)explode("-",$meta_key2)[1];


		
		$some_posts = new WP_Query( array(
    'date_query' => array(
        array(
            'year' => $year_start,
            'month' => $month_start,
            'compare' => '>=',
        ),
        array(
            'year' => $year_end,
            'month' => $month_end,
            'compare' => '<=',
        ),
        array(
            
            'compare' => 'BETWEEN',
        ),
    ),
    'posts_per_page' => 5,
) );

?>









		<div class="side1 col-md-6">
		<section id="primary" class="content-area">

 
 
        <form method="get" id="searchform" class="search-cont" action="" >
        <input id="s" style="" type="text" name="s" placeholder="search" value="" />
        <input style="width:95px; " type="DATE" name="sDate" class="datepicker form-control" placeholder="from" />
        <input style="width:95px;" type="DATE" name="eDate" class="datepicker2 form-control" placeholder="to"/>
        </form>
         


		<?php if ( $some_posts->have_posts() ) : ?>

			
				<h1 class="page-title"><?php _e( esc_html__( 'ძიების შედეგები: ', 'dgstz' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );
				?>

			<?php endwhile; ?>

			
		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
		</section>
		</div>

	
		

	<?php get_sidebar(); ?>	

	

<?php

get_footer();





